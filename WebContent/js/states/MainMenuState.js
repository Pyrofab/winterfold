var MainMenuState = {
    preload: function() {
        // Loading Font Style
        this.style = {
            font: "48px sumiremedium",
            fill: "#fff",
            boundsAlignH: "center",
            boundsAlignV: "middle"
        };

        // Background Animation
//        this.mainMenuBG = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'MMBackground');
//        this.mainMenuBG.anchor.setTo(0.5);
//        this.mainMenuBG.animations.add('background', [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 14, 14]);
//        this.mainMenuBG.animations.play('background', 10, true);
//        this.mainMenuBG.scale.setTo(2);
        		this.bgtest = this.add.sprite(0, 0, 'bgtest');

        // Winterfold Logo
        this.winterfoldLogo = this.add.sprite(this.game.world.width * 0.10, this.game.world.height * 0.15, 'winterfold-logo');
        this.winterfoldLogo.scale.setTo(1.5);
    },
    create: function () {

        game.add.plugin(Phaser.Plugin.Debug);
        game.add.plugin(Phaser.Plugin.Inspector);
        game.add.plugin(PhaserSuperStorage.StoragePlugin);
        game.add.plugin(PhaserInput.Plugin);

        // Music
        this.music = game.add.audio('fireside');
        this.music.play();
        this.hoversound = game.add.audio('mmhover');
        this.hoversound.volume = 60;
        this.options = this.game.add.group();
        this.options.inputEnabled = true;

        this.game.input.addMoveCallback(this.p, this);

        var cont = game.add.text(this.game.world.width * 0.10, this.game.world.height * 0.35, "Continue", this.style, this.options);
            cont.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            cont.alpha = 0.3;
            cont.inputEnabled = true;
        var newGame = game.add.text(this.game.world.width * 0.10, this.game.world.height * 0.40, "New", this.style, this.options);
            newGame.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            newGame.inputEnabled = true;
        var loadGame = game.add.text(this.game.world.width * 0.10, this.game.world.height * 0.45, "Load", this.style, this.options);
            loadGame.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            loadGame.inputEnabled = true;
        var credits = game.add.text(this.game.world.width * 0.10, this.game.world.height * 0.50, "Credits", this.style, this.options);
            credits.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            credits.inputEnabled = true;
            // credits.onInputOver.add(over, this);

    },
    update: function() {
        var self = this;
        var isPlaying = false;
        this.options.forEach(function(option) {

            // console.log(option.events);
            option.events.onInputOver.add(function() {
                if (option._text !== 'Continue') {
                    option.setText(option._text);
                    option.style.fill = "#a51411";
                    
                    if (!isPlaying) {
                    		self.hoversound.play();
                    				isPlaying = true;
                    } else {
                    		self.hoversound.restart();
                    		
                    }
                		

                }

            }, this)
            option.events.onInputOut.add(function() {
                if (option._text !== 'Continue') {
                    option.setText(option._text);
                    option.style.fill = '#fff';
                    self.hoversound.stop();
                    isPlaying = false;
                }
            }, this);
            option.events.onInputDown.add(function() {
                if (option._text === 'Continue') {
                    self.music.destroy();
                    self.state.start('TestState');
                }
            })

            // console.log(option.hitArea);
        })
    },
    p: function(pointer) {
    },
    up: function() {
        console.log('button up', arguments);
    },

    over: function() {
        console.log('button over');
    },

    out: function() {
        console.log('button out');
    },
    actionOnClick: function() {
        console.log("AYYY");
    }


};
